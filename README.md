# SpatialFeatureIdentification

this gitlab repository contains the R-code which was used for the Illustration and Application section in "R. Flury, F. Gerber, B. Schmid and R. Furrer 2020, Identification of Dominant Features in Spatial Data".

### how to install devel packages
  the three devel packages in the source/tar directory

 *  mresa_0.1.9000.tar.gz
 *  mrbsizeR_1.2.0.9000.tar.gz
 *  libdiversityindices_0.2.9000.tar.gz (not needed for illustration examples)

  can be installed in R with the function, e.g. `install.packages("sources/tar/mresa_0.1.9000.tar.gz", repos=NULL)`.

### how to access the data
 * the data for the three illustration examples in the R scripts `illustration*.R` is generated in the respective scripts.
 * the morphological trait data of the mountain Laegeren is not public yet, a link will be added here as soon as the owner of the data put these public.
 
### how to run code
 The analysis directory contains the R scripts,

 * to run the illustration examples, make sure all necessary packages are installed and the variable `dosample` is set to `TRUE`. Then the rest of the script can be run.
 
 * to run the application, check first the R script `00_setparam.R` to initialize parameters which are used for the following scripts. Then the enumerated scripts in `01_preprocessdata.R` to `06_diversityindices.R` can be used to reproduce results. The structure of the code can easy be adapted for other input data.


for further questions, contact roman.flury@math.uzh.ch or reinhard.furrer@math.uzh.ch
